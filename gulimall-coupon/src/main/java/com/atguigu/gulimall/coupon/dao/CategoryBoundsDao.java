package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CategoryBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品分类积分设置
 * 
 * @author shihaodong
 * @email 1946612448@qq.com
 * @date 2021-01-26 16:31:40
 */
@Mapper
public interface CategoryBoundsDao extends BaseMapper<CategoryBoundsEntity> {
	
}
