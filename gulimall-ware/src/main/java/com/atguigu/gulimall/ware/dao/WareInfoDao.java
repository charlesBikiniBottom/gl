package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author shihaodong
 * @email 1946612448@qq.com
 * @date 2021-01-26 17:23:35
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
