package com.atguigu.gulimall.gulimallproduct;

import com.atguigu.gulimall.product.GulimallProductApplication;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GulimallProductApplication.class})
class GulimallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Test
    void contextLoads() {

        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setDescript("小米");
        brandEntity.setName("小米");
        brandEntity.setShowStatus(0);
        brandService.save(brandEntity);
    }

    @Test
    public void findAll(){
        List<BrandEntity> list = brandService.list();
        list.stream().forEach(v->{
            System.out.println(v.toString());
        });
    }

    @Test
    public void findById(){
        List<BrandEntity> brandEntitys = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        brandEntitys.stream().forEach(v->{
            System.out.println(v.toString());
        });
    }

}
