package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author shihaodong
 * @email 1946612448@qq.com
 * @date 2021-01-26 14:40:33
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
