package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author shihaodong
 * @email 1946612448@qq.com
 * @date 2021-01-26 14:40:34
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
