package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author shihaodong
 * @email 1946612448@qq.com
 * @date 2021-01-26 16:44:55
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
